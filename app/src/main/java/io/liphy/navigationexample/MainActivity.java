package io.liphy.navigationexample;

import a.a.a.c.c;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.liphy.navigation.IndoorLocation;
import com.liphy.navigation.LiphyLocationService;
import com.liphy.navigation.LiphyState;
import com.liphy.navigation.bluetooth.BluetoothInfo;
import com.liphy.navigation.network.LiphyCloudManager;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.List;

import liphy.io.liphysdk.LightFlyCamera;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements LiphyLocationService.OnLiphyServiceListener,
        LiphyLocationService.OnBluetoothDeviceListener, EasyPermissions.PermissionCallbacks {

    private static final int LIPHY_PERMISSION_CODE = 100;
    private static final int OTHER_PERMISSION_CODE = 102;

    String[] liphyPerms = {
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    // liphy
    private LiphyLocationService liphyLocationService;
    private LiphyCloudManager liphyCloudManager;
    private boolean isLiphyServiceBound = false;
    private TextureView lightFlyTextureView;
    private TextView sdkVersionText;
    private TextView sdkExpiryDateText;

    // for any location updates
    private TextView providerText;
    private TextView latitudeText;
    private TextView longitudeText;
    private TextView floorText;
    private TextView buildingText;

    private TextView usernameText;

    private TextView accuracyText;
    private TextView orientationText;


    private TextView bluetoothNameText;

    private FloatingActionButton fabSwitchCam;
    private boolean frontCam = true;
    private ProgressDialog switchCamDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        lightFlyTextureView = findViewById(R.id.liphy_texture_view);

        // liphy
        Intent liphyLocationServiceIntent = new Intent(this, LiphyLocationService.class);
        this.bindService(liphyLocationServiceIntent, connection, Context.BIND_AUTO_CREATE);

        liphyCloudManager = LiphyCloudManager.getInstance();

        // initiate internal parameters for LiphyLocationService

        LiphyState.setMapReadyForCompass(true); // should be set as true inside onMapxusMapReady
        LiphyState.setPdrMode(true); // To Enable PDR

        // location update info
        providerText = findViewById(R.id.location_provider);
        latitudeText = findViewById(R.id.location_latitude);
        longitudeText = findViewById(R.id.location_longitude);
        floorText = findViewById(R.id.location_floor);
        buildingText = findViewById(R.id.location_building);

        accuracyText = findViewById(R.id.location_accuracy);
        orientationText = findViewById(R.id.location_orientation);

        // bluetooth
        bluetoothNameText = findViewById(R.id.bluetooth_name);

        // user info
        usernameText = findViewById(R.id.username);

        // log out for clearing memory
        liphyCloudManager.logout();

        /*
        test account
        username: example
        password: 123456
         */

        // To Login to the account
        liphyCloudManager.login("example", "123456", new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (e == null) {
                    usernameText.setText("Username: " + user.getUsername());
                } else {
                    usernameText.setText("Login Error: " + e.getLocalizedMessage());
                }
            }
        });

        // sdk info
        sdkVersionText = findViewById(R.id.sdk_version);
        sdkExpiryDateText = findViewById(R.id.sdk_expiry_date);

        // The button and method to Switch Camera for detecting VLC
        fabSwitchCam = findViewById(R.id.fab_switch_cam);
        fabSwitchCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frontCam = !frontCam;

                switchCamDialog = new ProgressDialog(MainActivity.this);
                switchCamDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                switchCamDialog.setTitle("Switching Camera");
                switchCamDialog.setMessage("Please wait for a moment...");
                switchCamDialog.setCancelable(false);
                switchCamDialog.show();
                switchCamera(frontCam);
                Handler dialogDimissHanlder = new Handler();
                dialogDimissHanlder.postDelayed(() -> {
                    switchCamDialog.dismiss();
                    if (frontCam) {
                        Toast.makeText(MainActivity.this, "Using Front Camera", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Using Back Camera", Toast.LENGTH_SHORT).show();
                    }
                }, 3000);

                if (frontCam) {
                    fabSwitchCam.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                } else {
                    fabSwitchCam.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSecondary)));
                }
            }
        });

        requestLocationPermission();
        requestOtherPermission();

    }

    @Override
    protected void onStart() {
        super.onStart();
        // turn on liphy & bluetooth at first
        if (isLiphyServiceBound) {
            if (LiphyState.isLiphyScan() && !liphyLocationService.isLiphyRunning() && !LiphyState.isFirstBluetoothLocUpdate()) {
                liphyLocationService.startLiPHYtracking();
            }

            if (LiphyState.isBluetoothSearch() && !liphyLocationService.isBluetoothRunning()) {
                liphyLocationService.startBluetoothSearch();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (isLiphyServiceBound) {
            liphyLocationService.stopLocationProviders();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.unbindService(connection);

        liphyLocationService.stopSelf();

    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            LiphyLocationService.LocalBinder binder = (LiphyLocationService.LocalBinder) service;
            liphyLocationService = binder.getService();
            liphyLocationService.getActivityAndTextureView(MainActivity.this, lightFlyTextureView);
            liphyLocationService.setAccessKeyForLiphySdk("XZejBz4VM8xq5MTRHSNMPHdmLgMb5FvV");
            liphyLocationService.registerOnLiphyServiceListener(MainActivity.this);
            liphyLocationService.registerOnBluetoothDeviceListener(MainActivity.this);

            if (EasyPermissions.hasPermissions(MainActivity.this, liphyPerms)) {
                liphyLocationService.startBluetoothSearch();
            }

            // sdk info
            sdkVersionText.setText("LiPHY SDK Version: " + liphyLocationService.liphySdkVersion());
            sdkExpiryDateText.setText("SDK Expiry Date: " + liphyLocationService.liphySdkExpiryDate());

            isLiphyServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            // see on destroyed
            liphyLocationService.unregisterOnLiphyServiceListener();
            liphyLocationService = null;
            isLiphyServiceBound = false;
        }
    };

    // compass orientation update callback
    @Override
    public void onOrientationUpdate(float orientation) {
        orientationText.setText("Orientation: " + String.valueOf(orientation));
    }

    @Override
    public void onCompassAccuracyChanged(int i) {

    }

    // Bluetooth detection callback
    @Override
    public void onLatestBluetoothInfoLoaded(BluetoothInfo bluetoothInfo) {
//        bluetoothNameText.setText("Latest bluetooth received: " + bluetoothInfo.getBluetoothName());
    }

    // Location Update Callback is received here
    @Override
    public void onLocationUpdate(IndoorLocation currentLocation) {

        Log.d("onLocationUpdate", "liphy state: " + LiphyState.getCurrentLocation().getBuildingName());

        Log.d("onLocationUpdate", "provider" + currentLocation.getProvider());

        providerText.setText("Provider: " + currentLocation.getProvider());
        latitudeText.setText("Latitude: " + String.valueOf(currentLocation.getLatitude()));
        longitudeText.setText("Longitude: " + String.valueOf(currentLocation.getLongitude()));
        floorText.setText("Floor: " + currentLocation.getFloor());
        buildingText.setText("Building: " + currentLocation.getBuildingName());

        bluetoothNameText.setText("BLE Name: " + currentLocation.getbleName());

        accuracyText.setText("Accuracy: " + currentLocation.getAccuracy());

        Log.d("location", "provider" + currentLocation.getProvider());
        Log.d("location", "accuracy" + currentLocation.getAccuracy());

        // necessary to enable PDR
        LiphyState.setFirstSignalReceived(true);

    }

    private void switchCamera(boolean frontCam) {
        if (frontCam) {
            liphyLocationService.switchLiPHYCamera(LiphyLocationService.LiPHYCameraFacing.FRONTSIDE);
        } else {
            liphyLocationService.switchLiPHYCamera(LiphyLocationService.LiPHYCameraFacing.BACKSIDE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        switch (requestCode) {
            case LIPHY_PERMISSION_CODE:
                if (liphyLocationService != null) {
                    if (!liphyLocationService.isBluetoothRunning()) {
                        liphyLocationService.startBluetoothSearch();
                    }
                }
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        switch (requestCode) {
            case LIPHY_PERMISSION_CODE:
                // close the app
                Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_LONG).show();
                finishAndRemoveTask();
                break;
        }
    }

    @AfterPermissionGranted(LIPHY_PERMISSION_CODE)
    private void requestLocationPermission() {
        if (!EasyPermissions.hasPermissions(this, liphyPerms)) {
            EasyPermissions.requestPermissions(this, "Request LiPHY Permissions", LIPHY_PERMISSION_CODE, liphyPerms);
        }
    }

    @AfterPermissionGranted(OTHER_PERMISSION_CODE)
    private void requestOtherPermission() {
        String[] otherPerms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE
        };
        if (!EasyPermissions.hasPermissions(this, otherPerms)) {
            EasyPermissions.requestPermissions(this, "Request Other Permissions", OTHER_PERMISSION_CODE, otherPerms);
        }
    }

}
