package io.liphy.navigationexample;

import android.app.Application;
import android.widget.Toast;

import com.liphy.navigation.LiphyState;
import com.liphy.navigation.network.LiphyCloudManager;
import com.parse.Parse;
import com.parse.ParseException;

import timber.log.Timber;

public class App extends Application {
    LiphyState liphyState;

    @Override
    public void onCreate() {
        super.onCreate();
        LiphyCloudManager.init(this);

        LiphyCloudManager liphyCloudManager = LiphyCloudManager.getInstance();

        liphyCloudManager.fetchLightBeaconsFromCloud();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

    }
}