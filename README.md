# LiPHY Navigation SDK for Android

## Introduction

- LiPHY Navigation SDK is based on combined usage of LiPHY, PDR and Bluetooth algorithms for indoor navigation. The SDK provides your app with live location udpates of the user via LiphyLocationService. The location service callback contains an IndoorLocation object that provides precise location information of the user. The IndoorLocation object contains longitude, latitude, building, and floor information. In addition, IndoorLocation object also specifies the type of location provider i.e., whether the location is provided via Bluetooth, PDR, or LiPHY (VLC). This information can be directly passed to a 3rd party digital map API to show the location of the user on a map or it can be used to trigger location related events in your app.

## Download and Install LiPHY Navigation SDK

- Following steps will tell you how to download the SDK and add it to your project directory.

### 1. Extract the SDK

- (Contact info@liphy.io for the SDK download link)
- Download the SDK from the provided link. It should contain two files, 'liphy-sdk.aar' and 'liphynavigation-sdk.aar'.
- Move your "liphy-sdk.aar" and "liphynavigation-sdk.arr" to "libs" under your module file

    ![libs directory](app/src/main/res/drawable/libs_directory.png)

## Configure your Android App

- Once you have successfully setup the SDK and added it to your project, you are ready to open your project in Android Studio and start using the SDK.
- Note that below steps will guide you on the minimum settings you need to configure in your project to use the SDK. For this example app, these steps are already performed, you just need to compile and run the app in Android Studio.

### 2. Add dependencies

1. build.gradle(project)

    ```xml
    allprojects {
        repositories {
            google()
            jcenter()
            flatDir {
                dirs 'libs'
            }
        }
    }
    ```

2. build.gradle(app)

    ```gradle
    apply plugin: 'com.android.application'

    android {
        compileSdkVersion ...
        buildToolsVersion ...
        defaultConfig {
            ...
        }
        compileOptions {
            sourceCompatibility = 1.8
            targetCompatibility = 1.8
        }
    }

    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.jar'])
        ...

        // liphy
        compile(name: 'liphy-sdk', ext: 'aar')
        compile(name: 'liphynavigation-sdk', ext: 'aar')
        implementation 'com.parse:parse-android:1.15.8'

        // others
        implementation "pub.devrel:easypermissions:3.0.0"
    }
    ```

### 3. Set up permissions and register services

```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="io.liphy.navigationexample">

		<!-- include/enable these permissions and features -->
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-feature
        android:name="android.hardware.bluetooth_le"
        android:required="false" />
    <uses-feature
        android:name="android.hardware.camera"
        android:required="false" />

    <application
        ....>

        <!-- Liphy access token -->
        <meta-data
            android:name="com.parse.SERVER_URL"
            android:value="@string/parse_server_url" />
        <meta-data
            android:name="com.parse.APPLICATION_ID"
            android:value="@string/parse_app_id" />

        <activity android:name=".MainActivity">
            ...
        </activity>

        <service
            android:name="com.liphy.navigation.bluetooth.BleDetectionService"
            android:enabled="true"
            android:exported="true" />

        <service android:name="com.liphy.navigation.LiphyLocationService"/>
    </application>

</manifest>
```

### 4. Add LiPHY keys in strings.xml

```java
<resources>
		...
    <string name="parse_server_url">http://lightfly.io/parse</string>
    <string name="parse_app_id">lightfly</string>
		...
</resources>
```

### 5. Layout file

- include a TextureView for LiPHY SDK

```java
<TextureView
    android:id="@+id/liphy_texture_view"
    android:layout_width="1dp"
    android:layout_height="1dp"
    android:visibility="visible"
    app:layout_constraintStart_toStartOf="parent"
    app:layout_constraintTop_toTopOf="parent" />
```

### 6. Bind with LiphyLocationService

```java
/**
 * Defines callbacks for service binding, passed to bindService()
 */
private ServiceConnection connection = new ServiceConnection() {
    @Override
    public void onServiceConnected(ComponentName className, IBinder service) {
		LiphyLocationService.LocalBinder binder = (LiphyLocationService.LocalBinder) service;
        liphyLocationService = binder.getService();
        liphyLocationService.getActivityAndTextureView(MainActivity.this, lightFlyTextureView);
        liphyLocationService.registerOnLiphyServiceListener(MainActivity.this);
        liphyLocationService.setAccessKeyForLiphySdk( .... put your liphy access key here ....);

				// start liphy & bluetooth
        if (EasyPermissions.hasPermissions(MainActivity.this, liphyPerms)) {
            if (LiphyState.isBluetoothSearch() && !liphyLocationService.isBluetoothRunning())
                liphyLocationService.startBluetoothSearch();
        }

        isLiphyServiceBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
        // see on destroyed
        liphyLocationService.unregisterOnLiphyServiceListener();
        liphyLocationService = null;
        isLiphyServiceBound = false;
    }
};
```

### 7. Lifecycle Methods

```java
@Override
protected void onCreate(Bundle savedInstanceState) {
		...

    lightFlyTextureView = findViewById(R.id.liphy_texture_view);
    Intent liphyLocationServiceIntent = new Intent(this, LiphyLocationService.class);
    this.bindService(liphyLocationServiceIntent, connection, Context.BIND_AUTO_CREATE);

		...
}

@Override
protected void onStart() {
    super.onStart();
    // turn on liphy & bluetooth at first
	if (isLiphyServiceBound) {
        if (!liphyLocationService.isBluetoothRunning()) {
            liphyLocationService.startBluetoothSearch();
        }
    }
}

@Override
public void onStop() {
    super.onStop();

    if (isLiphyServiceBound) {
        // liphyLocationService.stopBluetoothSearch(); // this line is deprecated
        liphyLocationService.stopLocationProvider();
    }
}
```

### 8. Location Update Callback

```java
@Override
    public void onLocationUpdate(IndoorLocation currentLocation) {

        // receive location callback from LiPHY/PDR/Bluetooth here
        // "IndoorLocation" object contains : Longitude, Latitude, Building, and Floor Information

    }

```
